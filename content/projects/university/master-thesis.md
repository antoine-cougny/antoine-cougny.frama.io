---
title: "Master Thesis: An Open-Market Economy for Multi Robots Systems Secured By Blockchain"
date: 2018-08-23T14:26:51+01:00
draft: false
slug: "master-thesis"
tags: ["project", "robotics", "ethereum", "blockchain", "ros", "python", "c++"]
mat: false
toc: true
---
# Presentation

## Links

Master Thesis available
[here](https://framagit.org/openmarketrobots_blockchain/DissertationProject)
and report [here](https://framagit.org/antoine-cougny/DissertationReport). This
work was conducted from May, 14th 2018 and August, 23rd 2018. It obtained the
grade of 79/100.

## Short Introduction

### Poster

This poster was used to support the defense of my work. Explanations lie bellow.

![Poster](/projects/university/Poster_v2_Final_100dpi.png)

### An Open-Market Economy?

Dividing tasks among a team is usually a great mean to improve the overall
efficiency of the team.  This fact is also valid for a Multi Robots System
(MRS). The paradigm then shifts from "how to perform this task?" to "who will
actually achieve it?".

On one side of the spectrum, there the fully centralized approach, in which a
central server computes the optimal solution (or at least tries to). This
relies on heavy networking and is not prone to change in the environments.

On the other side of the spectrum lies the fully distributed approach, swarms
robots for instance, which are very reactive to changes in the environment,
uses very little power and communication. However, approaching the best
solution is trickier as the team lacks a global overview of the tasks at hand.

A market approach is a trade-off between these two paradigms in which the
robots can buy and sell tasks to their peers. By acting in their self best
interest, they improve the efficiency of the whole team.  This approach
requires more communication than a fully distributed team of robots, but are
less power-hungry and more reactive to changes in the environment than the
optimal solution which was valid only at the moment it was calculated.


### Blockchain and Smart-contracts

![blockchain_figure](/projects/university/blockchain_figure.png)
Representation of a example blockchain for example.

A blockchain can be seen as an open and public ledger which every member of a
network stores and shares. This ledger is a database of transactions between
agents. Transactions are grouped into ”blocks” which contain the identifier of
the previous block, the information about a number of transactions and an
answer to a cryptographic problem which makes the block valid. Once a block is
added to a blockchain, it is impossible to modify. This technology shows that a
dis- tributed group of agents can reach an agreement without any central and
trusted intermediary.

A smart-contract can be seen as an autonomous script stored on the blockchain.
(Note that, not all blockchains support smart-contracts). This script describes
a complex set of activities which will be executed once a transaction will be
addressed to it. As a smart-contract deployment is stored in a block, the
script itself is immutable. This means, that once deployed, a contract cannot
be modified, and if well-written, is safe and secure to use.

### Combining Them

The novelty of this project resides in the merging of these two uprising
fields. The literature on MRS makes the assumption that robots are trustworthy
and reliable.  However, in the context of a democratization of such systems,
security, trust, accountability, and privacy become dealbreakers. This is where
the key advantages of blockchain are interesting: different robots, from
different manufacturers, do not necessarily trust each other. Yet, they could
reach a consensus and work together by trusting the cryptographic algorithm
more than their teammates.

## Achieved Results

![ServerRobots_SingleDApp_Horizontal](/projects/university/ServerRobots_SingleDApp_Horizontal.png)
Final architecture.

To test our system, we focus on solving a simplified Multiple Traveling
Salesman Problem (mTSP), in which a team has to visit a set of locations to
conduct some experiment or measurement. Every task is described as the
location, a waiting time (which a representation of the duration of the
conducted experiment) and a reward for performing the action. ROS was used
for this aspect of the project.

In this simplified model, the cost of the task was the traveling time to reach
the destination added to the duration of the experiment.

![0_gazebo_rviz](/projects/university/0_gazebo_rviz.png)
Gazebo & rviz at launch of an experiment.

At the beginning of the run, every robots check if the cost of achieving the
task they were initially attributed was higher than a tunable threshold. If so,
the task was considered not to be interesting enough for the robot and
auctioned to the network. Note that this auction system is executed on the ros
server pictured in the figure above.

If there was a, let's say closer, robot available, it would bid on the task and
would eventually earn it and then perform it. Afterwards, the robot signaled
the completion of the task in order to be paid.

![5_gazebo_rviz](/projects/university/5_gazebo_rviz.png)
Gazebo & rviz at the end of an experiment.

In our model, the blockchain, based on Ethereum, was used for logging purposes
and stored every transactions and tasks that happened in the MRS, as well as
handling the different robot accounts.

However, these were only simulation results and the blockchain was weakened in
order to get a prototype working in the given time deadline.

## Conclusion

This project was the first attempt, to my knowledge, of combining the
blockchain technology with the concept of Multi Robots System and the
Open-Market Economy paradigm. The conception of the system, based on the
Arcadia method, was centered around the idea of a modular and interoperable
system.

Future work focuses mainly in improving secrutiy, performing a physical
deployment and giving a more predominent role to the blockchain by moving the
auction system to a smart contract.


# Dissertation


Direct
[link](https://framagit.org/antoine-cougny/DissertationReport/-/blob/a1a4ee6731a93a30eb853945e211603a49369c41/Report_2018-08-23-FINAL-COUGNY-aoc2.pdf)
to the pdf.

The following sections are extracts from the dissertation. They are provided as
a more detailed TL;DR of the work.

## Abstract of the Paper

Research on Robotics is moving on the development of teams of robots in
order to increase their performance by dividing the task at hand. Among the
different coordination paradigms, the market-based approach is a trade-off
between a centralized solution and a fully distributed solution. However,
traditional solutions have made the assumption that robots are trustworthy,
reliable and accountable.  Due to its inherent features such as being
distributed, secured and shared, using a blockchain is the perfect
candidate to overcome these issues.

In this paper, we provide one of the first blockchain implementations on
top of an open-market economy using a single-round auction mechanism. Our
approach is built using ROS with a modular and interoperable design
and communicates an interface between the blockchain nodes, the robots and
the operator. Simulation results show that storing information such as
tasks, attributions and status is feasible.

We present a global view of methods and technologies underpinning an
open-market for robots, blockchain and a combined use of these
technologies.  Then, we present our architecture and the testing scenario.
We conducted tests in simulation and evaluated our system in regards to the
defined requirements, strengths and weaknesses.  This system was designed
by using a top-down system engineering methodology named Arcadia.

## Table of content of the Paper

**Abstract**

**1 - Problem Definition**
- Description of the Project
- Aims and Objectives
- Multirobot System (MRS)
- Blockchain
- Combining Them 
- Design Requirements
- Outline

**2 - Design Description**
- Methodology
- System Analysis
- Logical Architecture
- Different Deployment Options
- Use of the System 

**3 - Evaluation of the System**
- Testing of the Requirements 
- Assessment of the System
- Future Work

**4 - Conclusion**
- Objectives Summary
- Contribution to the Field

## Conclusion of the Paper

### Objectives Summary

This project is about adressing accountability, trust, reliability and privacy
issues in a team of robots by using a blockchain based solution. Overall, this
has successfully been implemented as shown in the previous sections.  The main
objectives have been met. Firstly, in terms of preparation, with the literature
review presented in Sections 1.3 to 1.5, we discussed methods and technologies
underpinning an open-market economy for MRS and then investigated different
options to implement the requirements we defined in Section 1.6.  Secondly, in
terms of development, a basic scenario including several robots and a basic
open-market using smart-contracts have successfully been developed. We decided
to build the open-market within ROS with the idea of the security provided by
the blockchain based solution being an optional add-on. However, no existing
framework could have been found on the Internet, which led us to develop our
own solution. Later, we spotted another opportunity to build the auction system
on the blockchain by using smart-contracts, which should lead to a better
solution in terms of security and reliability. This is because the execution of
the script would be distributed among the blockchain nodes.  Thirdly, in terms
of testing, we could only run tests on simulation as there were not enough
resources available to do a physical deployment on different machines and
robots. Nevertheless, we could still identify drawbacks and improvements in the
system as discussed in Section 3.3.  Finally, in terms of results analysis, we
provided a detailed evaluation in Chapter 3.  The implemented solution is
reliable at least in the context of the basic scenario it has been designed
for. However, there is a lot of room for improvements on the security aspects.

### Contribution to the Field

This project contributes in different ways to the fields of Robotics and
Blockchain.  It is one of the first projects bringing together a blockchain
with a Robotics project.  As mentioned earlier, traditional solutions have not
addressed security, trust and, accountability in their design. This project is
a first attempt at taking these into account because these will become
requirements systems aiming at large scale deployments.  To the best to our
knowledge, this is the first open-market economy based on ROS.  It was
developed with a modular design to make it easy to re-use and to extend.  For
instance, one could improve the metrics estimation by using a planner; or set
different tasks achievable by the robots.  This project is re-usable as it is
available under the MIT License. It is hence a suitable basis for any further
work in the same context. 
