---
title: "Introduction to Computer Vision with OpenCV - Projects"
date: 2020-02-26T14:26:51+01:00
slug: "opencv-project-presentation"
description: "A short summary of my different project made with OpenCV"
tags: ["project", "opencv", "python", "c++"]
mat: false
toc: true
draft: false
---

# Presentation
These are projects I did for the OpenCV.org course on
Computer Vision. Each of them were completed in February 2020 and led to the
grade of 90% in C++ and 100% in Python.

Link to the repository [here](https://forge.tedomum.net/thelostwanderer/OpenCV_102-103_ComputerVisionIntroduction_Projects).


# Projects

## Blemish Removal (C++)

Select a blemish to remove by clicking on it. Will select the best patch around
and apply it on the selected area.

![Preview](/projects/opencv/intro_to_computer_vision_opencv/prj1.2_preview_216p.gif)

## Document Scanner (Python)

Open a picture of a document, adjust the contrast and brightness to perform
an extraction with a homography. 

![Preview 1](/projects/opencv/intro_to_computer_vision_opencv/prj3_preview_1_216p.gif)

Ability to adjust the corner if the detection failed and save the extracted
document.

![Preview 2](/projects/opencv/intro_to_computer_vision_opencv/prj3_preview_2_216p.gif)

## Detection & Tracking (C++ & Python)

This project consisted in detection a soccer ball and track it as long as
possible. When the tracker is unable to continue, the detector should kick-in
every 5 frames. Tracker is KCF and detector is YoLo V3.

![Video](/projects/opencv/intro_to_computer_vision_opencv/prj4_preview_216p.gif)
