---
title: "DeepLearning with Pytorch - Projects"
date: 2020-12-29T15:16:21+01:00
slug: "pytorch-project-presentation"
description: "A short summary of my different project made with PyTorch"
tags: ["project", "opencv", "python", "pytorch"]
mat: false
toc: true
draft: false
---


This repository will host the projects I did for the OpenCV.org courses on
Deeplearning with Pytorch. Each of them were achieved in Fall 2020.

Link to the repository [here](https://forge.tedomum.net/thelostwanderer/OpenCV_106_DeepLearningWithPytorch_Projects).

## Project 1  - Classification from Scratch - 100%

This project consisted in implementing the different steps of the training
pipeline used to train an image classifier from scratch. The aim of the project
was then to classify pictures of dogs, cats and pandas in their respective
class. Simple task for a human, much harder for a machine.


It consists of two Jupyter notebooks, the first one focusing on:
- Step 1 - Understand your problem
- Step 2A - Get the data
- Step 2B - Explore & Understand your data
- Step 2C - Create a sample data from the dataset
- Step 3 - Data Preparation
- Step 4 - Train a simple model on sample data and check the pipeline before
proceeding to train the full network

The second part focuses on defining the actual model, training it and improving
it to reach the minimal validation accuracy of 85%.

The training graph looks like this:

![training graph](/projects/opencv/deeplearning_with_pytorch/prj1_final_training_acc_graph.png)

The confusion matrix:

![confusion_matrix](/projects/opencv/deeplearning_with_pytorch/prj1_final_confusion_matrix.png)

An inference on a sample of data from the training set:

![sample_inference](/projects/opencv/deeplearning_with_pytorch/prj1_final_inference_on_samples.png)

## Project 3 - Object Detection - 100%

[Wikipedia](https://en.wikipedia.org/wiki/Electronic_toll_collection) states that
> Electronic toll collection (ETC) is a wireless system to automatically
> collect the usage fee or toll charged to vehicles using toll roads, HOV
> lanes, toll bridges, and toll tunnels.

ANPR (Automatic Number Plate Recognition) is one of the challenges encountered
by the ETC. This project consisted in solving this problem.

Using the *Vehicle registration plate* public dataset and the `detectron2`
framework developed by Facebook AI Research, I trained a neural network
to detect license plates.

Here are a sample of the result:

![first_car](/projects/opencv/deeplearning_with_pytorch/prj3_c3-w10-p1.png)
![second_car](/projects/opencv/deeplearning_with_pytorch/prj3_c3-w10-p2.png)
![third_car](/projects/opencv/deeplearning_with_pytorch/prj3_c3-w10-p3.png)

The final task consisted in processing the following video and inferring each
frames in order to detect and locate the license plate on the different frames.


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts
allow-popups"
src="https://p.eertu.be/videos/embed/09295ef4-4f9f-40c2-bb15-84957c98468b?start=0s"
frameborder="0" allowfullscreen></iframe>


